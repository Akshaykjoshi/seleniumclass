package login;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Next {
	static WebDriver driver;

	@BeforeTest
	public static void testLogin() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\User\\eclipse-workspace\\Selenium Class\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://employeemgmt.employee.teknotrait.com/");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("dharmendra.patel264@gmail.com");
		driver.findElement(By.xpath("//input[@name='pwd']")).sendKeys("123456");
		driver.findElement(By.xpath("//button[@class='btn btn-primary hidden-xs signin']")).click();
		driver.findElement(By.xpath("//a[@class='Vendor']")).click();
	}

	@Test
	public static void preButton() {
		driver.findElement(
				By.xpath("/html[1]/body[1]/span[1]/div[3]/div[1]/div[1]/div[3]/div[2]/div[1]/ul[1]/li[4]/a[1]"))
				.click();

	}

	@AfterTest
	public static void quit() throws Exception {
		Thread.sleep(10000);
		driver.quit();
	}
}
