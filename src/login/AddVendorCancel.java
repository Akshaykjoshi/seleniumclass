package login;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AddVendorCancel {
	static WebDriver driver;
	@BeforeTest
	public static void testLogin() throws Exception
	{
		System.setProperty("webdriver.chrome.driver","C:\\Users\\User\\eclipse-workspace\\Selenium Class\\driver\\chromedriver.exe");
        driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://employeemgmt.employee.teknotrait.com/");
        driver.findElement(By.xpath("//input[@name='username']")).sendKeys("dharmendra.patel264@gmail.com");
        driver.findElement(By.xpath("//input[@name='pwd']")).sendKeys("123456");
        driver.findElement(By.xpath("//button[@class='btn btn-primary hidden-xs signin']")).click();
        driver.findElement(By.xpath("//a[@class='Vendor']")).click();
	}


@Test
 public static void addv()
 {
	driver.findElement(By.xpath("//button[@id='add_vendor']")).click(); 
	driver.findElement(By.xpath("//input[@id='vendor']")).sendKeys("ABCD");
	driver.findElement(By.xpath("//input[@id='business']")).sendKeys("STY.SOLVE");
	driver.findElement(By.xpath("//textarea[@id='address']")).sendKeys("use the address 1010 xcyv");
	driver.findElement(By.xpath("//input[@id='mobile']")).sendKeys("0234567890");
	driver.findElement(By.xpath("//input[@id='email']")).sendKeys("a@z.co");
	driver.findElement(By.xpath("//div[@id='vendorModal']//button[@class='btn btn-danger'][contains(text(),'Close')]")).click();
 }
@AfterTest

	public static void quit() throws Exception
	{
		Thread.sleep(4000);
        driver.quit();   	
}
 }

