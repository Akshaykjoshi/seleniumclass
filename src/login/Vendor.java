package login;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Vendor {
	static WebDriver driver;

	@BeforeTest
	public static void testLogin() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\User\\eclipse-workspace\\Selenium Class\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://employeemgmt.employee.teknotrait.com/");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("dharmendra.patel264@gmail.com");
		driver.findElement(By.xpath("//input[@name='pwd']")).sendKeys("123456");
		driver.findElement(By.xpath("//button[@class='btn btn-primary hidden-xs signin']")).click();

	}

	@Test
	public static void testVendorButton() {
		// Clicking on vendor button
		driver.findElement(By.xpath("//a[@class='Vendor']")).click();

		// Verification
		String pageName = driver.findElement(By.id("action_name")).getText();
		Assert.assertEquals(pageName, "Vendor");

	}

	@AfterTest
	public static void quit() throws Exception {
		Thread.sleep(4000);
		driver.quit();
	}
}